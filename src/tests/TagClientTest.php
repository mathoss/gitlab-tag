<?php

namespace Ma\GitlabTag\tests;

use GuzzleHttp\Exception\ClientException;
use Ma\GitlabTag\Exceptions\NotFoundException;
use Ma\GitlabTag\TagClient;
use PHPUnit\Framework\TestCase;

class TagClientTest extends TestCase
{
    public function testInvaildProjectName()
    {
        $this->expectException(NotFoundException::class);
        $this->expectExceptionMessage('没有找到项目配置');
        new TagClient('aaa');
        $this->fail('Failed to run testInvaildProjectName test');
    }

    public function testGetTagsApi()
    {
        $client = new TagClient('anole');
        $tags = $client->getTags();
        $this->assertIsArray($tags);
        $this->assertNotEmpty($tags);
    }

    public function testGetTagApiOnInvaildTagName()
    {
        $client = new TagClient('anole');
        $this->expectException(ClientException::class);
        $client->getTag('foo');
        $this->fail('Failed to run testGetTagApiByInvaildTagName test');
    }

    public function testGetTagApi()
    {
        $client = new TagClient('anole');
        $tag = $client->getTag('docker-build-test-0.8.74');
        $this->assertIsArray($tag);
        $this->assertNotEmpty($tag);
    }

    public function testCreateTagApi()
    {
        $client = new TagClient('usercenter');
        $tag = $client->createTag('0.3.36');
        $this->assertNotEmpty($tag);
    }

    public function testCreateTagApiOnTagNameExist()
    {
        $client = new TagClient('anole');
        $this->expectException(ClientException::class);
        $client->createTag('0.8.74');
        $this->fail('Failed to run testCreateTagApiOnTagNameExist test');
    }

    public function testCreateTagByAutoNameApi()
    {
        $client = new TagClient('usercenter');
        $response = $client->createTagByAutoName();
        if (!is_array($response) || !$response[0] || !$response[1]) {
            $this->fail('Failed to run testCreateTagByAutoNameApi test');
        }
        $this->assertIsArray($response);
        $this->assertNotEmpty($response);
    }
}