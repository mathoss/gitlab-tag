<?php

return [
    'token' => '',
    'gitlab_host' => '',
    'config' => [
        'project_a' => [
            'project_id' => 6464,
            'tag_name_prefix' => 'docker-build-test-',
            'branch' => 'test',
        ],
    ]
];
