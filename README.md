

## 介绍

gitlab的tag api，调用接口可以很方便的推送tag到gitlab服务器。

## 安装

```bash
composer require maxiao64/gitlab-tag
```

## 使用
### 配置
打开/vendor/maxiao64/gitlab-tag/src/config.php
添加参数
参数说明：

token: gitlab的access_token

创建token的步骤：Settings -> Access Tokens -> 随便填写一个名字,Scopes选中api -> Create personal access token

gitlab_host: gitlab的域名 + 端口号

config：项目配置的列表
```php
'project_a' => [
    'project_id' => 6464,
    'tag_name_prefix' => 'docker-build-test-',
    'branch' => 'test',
],
```
project_a 为项目名字，这个在执行命令的时候会用到

project_id 为项目的id，可以在项目主页查看到

tag_name_prefix 为分支名的前缀

branch 为打包的分支

### 功能：
#### 创建最新的tag，tag的版本为当前最大的版本 + 1
```php
$projectName = 'project_a';
$client = new \Ma\GitlabTag\TagClient($projectName);
$tag = $client->createTagByAutoName();
```
返回的$tag为新创建的tag信息

#### 根据tag名字创建tag
```php
$projectName = 'project_a';
$client = new \Ma\GitlabTag\TagClient($projectName);
$tag = $client->createTag('docker-build-test-0.0.1');
```
返回的$tag为新创建的tag信息

### license
MIT


